#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#define IO_DMA_RESET			_IO(0xFF, 0)
#define IO_DMA_START_COPY		_IO(0xFF, 1)
#define IO_DMA_START_TOUPPER	_IO(0xFF, 2)
#define IO_DMA_GET_STATS		_IO(0xFF, 3)
#define IO_DMA_SET_IF			_IO(0xFF, 4)
#define IO_DMA_SET_IE			_IO(0xFF, 5)
#define IO_DMA_SETBURSTSZR		_IO(0xFF, 6)
#define IO_DMA_SETBURSTSZW		_IO(0xFF, 7)

#define BUF_SIZE				1024
#define NNEURONS				128
#define NINPUTS					64
#define NRECURRENT				3

int fd;

void initialize_weight_buffer(int *buf){
	int i;
	for(i = 0; i<NNEURONS; i++){
		if(i%2 == 1) buf[i] = -i;
		else buf[i] = i;
	}
}

void initialize_inputs(int *buf){
	int i;
	for(i = 0; i<NINPUTS; i++){
		if(i%4 == 0) buf[i] = 0;
		else if(i%4 == 1) buf[i] = 1;
		else if(i%4 == 2) buf[i] = 2;
		else buf[i] = 3;
	}
}

// NRECURRENT <= NNEURONS
int simulate_neural_network_withDMA(){
	int x[NINPUTS]; //Inputs of the neural network
	int wi[NNEURONS]; //Weights of the perceptrons
	int yi[NNEURONS]; //Outputs of the neural network
	int acc[NNEURONS];
	int i, j, k;

	lseek(fd, 0, SEEK_SET);
	//Reading the weights from the DMA
	if (read(fd, wi, NNEURONS*sizeof(int)) < 0) {
		printf("Device read error\n");
		return 2;
	}

	//Reading the inputs from the DMA
	if (read(fd, x, NINPUTS*sizeof(int)) < 0) {
		printf("Device read error\n");
		return 2;
	}

	for (i = 0; i < NNEURONS; i++) acc[i] = 0;

	//All inputs are processed by all neurons
	for (i = 0; i < NNEURONS; i++){
		for (j = 0; j < NINPUTS; j++) 
			acc[i] += x[j]*wi[i];
	}

	//Output evaluation. fesc
	for (i = 0; i < NNEURONS; i++){
		if(acc[i] > 0) yi[i] = 1;
		else yi[i] = 0;
	}

	//The recurrent inputs are processed by the neurons
	for (i = 0; i < NNEURONS; i++){
		for (j = 0; j < NRECURRENT; j++)
			acc[i] += yi[j]*wi[i];
	}

	//Output evaluation. fesc
	for (i = 0; i < NNEURONS; i++){
		if(acc[i] > 0) yi[i] = 1;
		else yi[i] = 0;
	}

	//Writing the neuron accumulators into the DMA
	if (write(fd, yi, NNEURONS*sizeof(int)) < 0) {
		printf("Device write error\n");
		return 2;
	}

	return 1;
}

// NRECURRENT <= NNEURONS
int simulate_neural_network(){
	int x[NINPUTS]; //Inputs of the neural network
	int wi[NNEURONS]; //Weights of the perceptrons
	int yi[NNEURONS]; //Outputs of the neural network
	int acc[NNEURONS];
	int i, j, k;

	initialize_weight_buffer(wi);
	initialize_inputs(x);

	for (i = 0; i < NNEURONS; i++) acc[i] = 0;

	//All inputs are processed by all neurons
	for (i = 0; i < NNEURONS; i++){
		for (j = 0; j < NINPUTS; j++) 
			acc[i] += x[j]*wi[i];
	}

	//Output evaluation. fesc
	for (i = 0; i < NNEURONS; i++){
		if(acc[i] > 0) yi[i] = 1;
		else yi[i] = 0;
	}

	//The recurrent inputs are processed by the neurons
	for (i = 0; i < NNEURONS; i++){
		for (j = 0; j < NRECURRENT; j++)
			acc[i] += yi[j]*wi[i];
	}

	//Output evaluation. fesc
	for (i = 0; i < NNEURONS; i++){
		if(acc[i] > 0) yi[i] = 1;
		else yi[i] = 0;
	}

	//Printing the output
	for (i=0; i < NNEURONS; i++) printf("Output of neuron %d: %d\n", i, yi[i]);

	return 1;
}

int main(int argc, char *argv[])
{
	int weight_buf[NNEURONS];
	int input_buf[NINPUTS];
	int acc[NNEURONS];
	int i, cnt;

	if(argc != 2 || !(strcmp(argv[1], "-s") != 0 || strcmp(argv[1], "-sd") != 0 || strcmp(argv[1], "-f") != 0)){
		printf("Error in program arguments\n");
		printf("Usage:\n");
		printf("-s : Simulation without dma\n");
		printf("-sd : Simulation with dma\n");
		printf("-f : Execution in fpga\n");
		return 0;
	}

	if(strcmp(argv[1], "-s") == 0){
		simulate_neural_network();
		return 1;
	}

	//DMA Execution:
	//Should be changed to the proper kernel module name
	fd  = open("/dev/io_dma_char", O_RDWR);
	if (fd < 0) {
		printf("Can't open device\n");
		return 1;
	}
	
	/* Interrupt enable */
	ioctl(fd, IO_DMA_SET_IE, 1);
	
	if (ioctl(fd, IO_DMA_RESET, BUF_SIZE*sizeof(int)) < 0) {
		printf("DMA reset error\n");
		return 1;
	}

	initialize_weight_buffer(weight_buf);
	initialize_inputs(input_buf);

	//Writing the weights into the DMA
	if (write(fd, weight_buf, NNEURONS*sizeof(int)) < 0) {
		printf("Device write error\n");
		return 2;
	}

	//Writing the inputs into the DMA
	if (write(fd, input_buf, NINPUTS*sizeof(int)) < 0) {
		printf("Device write error\n");
		return 2;
	}		

	/*
	 * Should be changed to the synchronization with the FPGA
	 */
	//ioctl(fd, IO_DMA_START_COPY);
	if(strcmp(argv[1], "-sd")) simulate_neural_network_withDMA();
	
	lseek(fd, (NINPUTS+NNEURONS)*sizeof(int), SEEK_SET);

	//Getting the accumulators from the DMA
	if (read(fd, acc, NNEURONS*sizeof(int)) < 0) {
		printf("Device read error\n");
		return 2;
	}
	
	return 0;
}

