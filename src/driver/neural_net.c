/* Kernel platform driver for neutral network */

/*
* USAGE:
* 1. set burst sizes, enable interrupt
  2. set Y_SIZE, R_SIZE and L_SIZE, K_HEIGHT and K_WIDTH if needed
* 3. set net mode and number of iterations
* 4. write data with write()
* 5. ioctl(START_NET)
* 6. read results with read()
*/


#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

/* Needed for copy_from_user */
#include <asm/io.h>
#include <linux/ioctl.h>
#include <linux/string.h>

#include <linux/platform_device.h> /* Needed for Platform Driver Functions */
#include <linux/device.h>			/* struct class */

/* kmalloc, kfree */
#include <linux/slab.h>

#include <linux/interrupt.h>
#include <linux/dma-mapping.h>

#define DRIVER_NAME "neuralnet"

/* registers */
#define REG_X_ADDR 		0x00
#define REG_Y_ADDR 		0x04
#define REG_SIZE_A 		0x08
#define REG_SIZE_B 		0x0C
#define REG_SIZE_C 		0x10
#define REG_NET_CTRL 	0x14
#define REG_STATUS 		0x18
#define REG_DMA_PAR 	0x1C

/* iotcl requests */
#define NN_RESET			_IO(0xFF, 0)
#define NN_START_COPY		_IO(0xFF, 1)
#define NN_START_TOUPPER	_IO(0xFF, 2)
#define NN_START_NET		_IO(0xFF, 3)
#define NN_READ_IF			_IO(0xFF, 4)
#define NN_ENABLE_IRQ		_IO(0xFF, 5)
#define NN_SET_BURST_R		_IO(0xFF, 6)
#define NN_SET_BURST_W		_IO(0xFF, 7)

#define NN_SET_Y_SIZE 		_IO(0xFF, 8)
#define NN_SET_R_SIZE		_IO(0xFF, 9)
#define NN_SET_L_SIZE		_IO(0xFF, 10)
#define NN_SET_K_HEIGHT		_IO(0xFF, 11)
#define NN_SET_K_WIDTH		_IO(0xFF, 12)
#define NN_SET_NET_MODE		_IO(0xFF, 13)
#define NN_SET_ITERATIONS	_IO(0xFF, 14)
#define NN_R_SIZE			_IO(0xFF, 15)
#define NN_L_SIZE			_IO(0xFF, 16)
#define NN_K_HEIGHT			_IO(0xFF, 17)
#define NN_K_WIDTH			_IO(0xFF, 18)
#define NN_DMA_CLOCKS 		_IO(0xFF, 19)
#define NN_DIAG_ON			_IO(0xFF, 20)
#define NN_DIAG_OFF			_IO(0xFF, 21)

unsigned long *base_addr;	/* Virtual Base Address */
static struct resource *res;		/* Device Resource Structure */
unsigned long remap_size;	/* Device Memory Size */
static unsigned int irq_nr;	/* IRQ number */

/* DMA buffer kernel and bus addresses */
struct neural_net_buf
{
	unsigned char	*cpu_addr;
	unsigned int	pos;
	dma_addr_t		dma_addr;
	size_t			size;
};

static struct neural_net_buf buffer_in;
static struct neural_net_buf buffer_out;

struct device			*neural_net_dev;
static struct class		*neural_net_class;
static int				char_major;

static DECLARE_WAIT_QUEUE_HEAD(wq);
static int wait_flg = 0;

static unsigned _net_mode = 0x2;
static unsigned _net_iter = 0;
static unsigned _net_y_size = 0;

static irqreturn_t neural_net_inthandler(int irq, void *dev)
{
	wait_flg = 1;
	wake_up_interruptible(&wq);
	
	/* Zero IF */
	iowrite8(0, (unsigned char *)base_addr + REG_STATUS);
	
	/*
	dma_unmap_single((struct device *) dev, dma_addr, dma_size, DMA_FROM_DEVICE);
	*/
	
	return IRQ_HANDLED;
}

static ssize_t neural_net_read(struct file *file, char __user * buf, size_t count, loff_t * offset)
{
	unsigned int cnt;
	unsigned int bytes_left;
	
	//int i;
	
	wait_event_interruptible(wq, wait_flg != 0);
	
	bytes_left = (*offset >= buffer_out.size) ? 0 : buffer_out.size - *offset;
	cnt = (bytes_left > count) ? count : bytes_left;
	
	if (cnt == 0) {
		return 0;
	}
	
	if (copy_to_user(buf, (const char *) (buffer_out.cpu_addr + *offset), cnt)) {
		return -EFAULT;
	}
	
	*offset += cnt;
	
	printk(KERN_INFO "read: cnt = %u\n", cnt);
	printk(KERN_INFO "read: buffer_out.size = %u\n", buffer_out.size);
	
	/*
	for (i = 0; i < buffer_out.size + 8; i++)
		printk(KERN_INFO "%u ", buffer_out.cpu_addr[i]);
	*/
	
	return cnt;
}

static ssize_t neural_net_write(struct file *file, const char __user * buf, size_t count, loff_t * offset)
{	
	unsigned int cnt;
	unsigned int bytes_left = (*offset >= buffer_in.size) ? 0 : buffer_in.size - *offset;
	
	cnt = (bytes_left > count) ? count : bytes_left;
	
	if (cnt == 0) {
		return 0;
	}
	
	if (copy_from_user(buffer_in.cpu_addr + *offset, (const char *) buf, cnt)) {
		return -EFAULT;
	}
	
	buffer_in.pos += cnt;
	*offset += cnt;
	
	printk(KERN_INFO "write: cnt = %u\n", cnt);
	
	return cnt;
}

static long neural_net_ioctl(struct file *fp, unsigned int request, unsigned long param)
{
	int i;
	switch (request) {
		case NN_RESET:
			if (buffer_in.size)
				dma_free_coherent(neural_net_dev, buffer_in.size, buffer_in.cpu_addr, buffer_in.dma_addr);
			buffer_in.size = param;
			buffer_in.cpu_addr = dma_alloc_coherent(neural_net_dev, buffer_in.size, &buffer_in.dma_addr, GFP_DMA);
			if (!buffer_in.cpu_addr)
				return -EFAULT;
			buffer_in.pos = 0;
			break;
		
		/* legacy functions */	
		case NN_START_COPY:
		case NN_START_TOUPPER:
			if (buffer_in.pos == 0)
				break;
			
			wait_flg = 0;
			if (buffer_out.size)
				dma_free_coherent(neural_net_dev, buffer_out.size, buffer_out.cpu_addr, buffer_out.dma_addr);
			/* pos		size
			 * 0		0		0 + 0
			 * 1-4		4		1 + 3, 2 + 2, 3 + 1, 4 + 0
			 * 5-8		8		5 + 3, 6 + 2, ...
			 */
			buffer_out.size = (buffer_in.pos & 3) ? buffer_in.pos + (4 - (buffer_in.pos & 3)) : buffer_in.pos;
			buffer_out.cpu_addr = dma_alloc_coherent(neural_net_dev, buffer_out.size, &buffer_out.dma_addr, GFP_DMA);
			if (!buffer_out.cpu_addr)
				return -EFAULT;
			buffer_out.pos = 0;
			
			for (i = 0; i < buffer_out.size; i++)
				buffer_out.cpu_addr[i] = 0xFF;
			
			iowrite32(buffer_in.dma_addr, (unsigned char *)base_addr + REG_X_ADDR);
			iowrite32(buffer_out.dma_addr, (unsigned char *)base_addr + REG_Y_ADDR);
			iowrite16((buffer_in.pos - 1) / 4 + 1, (unsigned char *)base_addr + REG_SIZE_A);
			iowrite16((buffer_in.pos - 1) / 4 + 1, (unsigned char *)base_addr + REG_SIZE_A + 2);
			wmb();	/* write memory barrier */
			/* Select a function */
			iowrite8(request - 1, (unsigned char *)base_addr + REG_DMA_PAR + 2);
			/* Enable */
			iowrite8(0x1, (unsigned char *)base_addr + REG_NET_CTRL);
			break;

		case NN_START_NET:
			if (buffer_in.pos == 0)
				break;
			
			wait_flg = 0;
			/* free old */
			if (buffer_out.size)
				dma_free_coherent(neural_net_dev, buffer_out.size, buffer_out.cpu_addr, buffer_out.dma_addr);
			/* allocate new */
			buffer_out.size = _net_y_size ? _net_y_size : buffer_in.pos;
			buffer_out.cpu_addr = dma_alloc_coherent(neural_net_dev, buffer_out.size, &buffer_out.dma_addr, GFP_DMA);
			if (!buffer_out.cpu_addr)
				return -EFAULT;
			buffer_out.pos = 0;
			
			iowrite32(buffer_in.dma_addr, (unsigned char *)base_addr + REG_X_ADDR);
			iowrite32(buffer_out.dma_addr, (unsigned char *)base_addr + REG_Y_ADDR);
			iowrite16((buffer_in.pos - 1) / 4 + 1, (unsigned char *)base_addr + REG_SIZE_A);
			iowrite16(buffer_out.size, (unsigned char *)base_addr + REG_SIZE_A + 2);
			wmb();	/* write memory barrier */
			/* Select the neural net function */
			iowrite8(0x10, (unsigned char *)base_addr + REG_DMA_PAR + 2);
			/* Enable */
			iowrite32(_net_iter | _net_mode | 0x1, (unsigned char *)base_addr + REG_NET_CTRL);
			break;
			
		case NN_READ_IF:
			*((unsigned char*)param) = ioread8((unsigned char *)base_addr + REG_STATUS) ? 1 : 0; 
			
		case NN_ENABLE_IRQ:
			iowrite8(param ? 1 : 0, (unsigned char *)base_addr + REG_STATUS + 1);
			break;
			
		case NN_SET_BURST_R:
			iowrite8((unsigned char) param, (unsigned char *)base_addr + REG_DMA_PAR);
			break;
		
		case NN_SET_BURST_W:
			iowrite8((unsigned char) param, (unsigned char *)base_addr + REG_DMA_PAR + 1);
			break;
		
		case NN_SET_R_SIZE:
			iowrite16(param, (unsigned char *)base_addr + REG_SIZE_B);
			break;

		case NN_SET_L_SIZE:
			iowrite16(param, (unsigned char *)base_addr + REG_SIZE_C + 2);
			break;

		case NN_SET_K_HEIGHT:
			iowrite8(param, (unsigned char *)base_addr + REG_SIZE_C + 1);
			break;

		case NN_SET_K_WIDTH:
			iowrite8(param, (unsigned char *)base_addr + REG_SIZE_C);
			break;

		case NN_SET_NET_MODE:
			_net_mode = (param & 0xF) << 4;
			break;

		case NN_SET_ITERATIONS:
			_net_iter = (param & 0xFFFFFF) << 8;
			break;

		case NN_R_SIZE:
			i = ioread16((unsigned char *)base_addr + REG_SIZE_B);
			copy_to_user((void*)param, &i, 2);
			break;

		case NN_L_SIZE:
			i = ioread16((unsigned char *)base_addr + REG_SIZE_C + 2);
			copy_to_user((void*)param, &i, 2);
			break;

		case NN_K_HEIGHT:
			i = ioread8((unsigned char *)base_addr + REG_SIZE_C + 1);
			copy_to_user((void*)param, &i, 1);
			break;

		case NN_K_WIDTH:
			i = ioread8((unsigned char *)base_addr + REG_SIZE_C);
			copy_to_user((void*)param, &i, 1);
			break;

		case NN_DMA_CLOCKS:
			i = ioread32((unsigned char *)base_addr + REG_DMA_PAR);
			copy_to_user((void*)param, &i, 4);
			break;

		case NN_DIAG_ON:
			iowrite32(_net_iter | _net_mode | 0x8, (unsigned char *)base_addr + REG_NET_CTRL);
			break;

		case NN_DIAG_OFF:
			iowrite32(_net_iter | _net_mode | 0x0, (unsigned char *)base_addr + REG_NET_CTRL);
			break;

		case NN_SET_Y_SIZE:
			_net_y_size = param & 0xFFFF;
			break;

		default:
			return -EINVAL;
			break;
	}
	
	return 0;
}

static loff_t neural_net_llseek(struct file *filp, loff_t off, int whence)
{
    loff_t newpos;

    switch (whence) {
      case 0: /* SEEK_SET */
        newpos = off;
        break;

      case 1: /* SEEK_CUR */
        newpos = buffer_in.pos + off;
        break;

      case 2: /* SEEK_END */
        newpos = buffer_in.size + off;
        break;

      default: /* can't happen */
        return -EINVAL;
		break;
	}
	
	if (newpos < 0) return -EINVAL;
	buffer_in.pos = newpos;
	filp->f_pos = newpos;
	
	return newpos;
}

static int neural_net_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int neural_net_release(struct inode *inode, struct file *file)
{
	buffer_in.pos = 0;
	buffer_out.pos = 0;
	
	return 0;
}

/* File operations */
static const struct file_operations neural_net_fops = {
	.owner = THIS_MODULE,
	.open = neural_net_open,
	.read = neural_net_read,
	.write = neural_net_write,
	.llseek = neural_net_llseek,
	.unlocked_ioctl = neural_net_ioctl,
	.release = neural_net_release
};

/* Module remove function */
static int neural_net_remove(struct platform_device *pdev)
{
	/* TODO device reset */
	
	/* Dealloc character device */
	device_destroy(neural_net_class, MKDEV(char_major, 0));
	class_unregister(neural_net_class);
	class_destroy(neural_net_class);
	unregister_chrdev(char_major, DRIVER_NAME);
	
	printk (KERN_INFO "%s: character device unregister, major %d\n", DRIVER_NAME, char_major);
	
	/* Release IRQ */
	/*free_irq(29, &pdev->dev); */
	
	if (buffer_in.size)
		dma_free_coherent(neural_net_dev, buffer_in.size, buffer_in.cpu_addr, buffer_in.dma_addr);
	
	if (buffer_out.size)
		dma_free_coherent(neural_net_dev, buffer_out.size, buffer_out.cpu_addr, buffer_out.dma_addr);
	
	iounmap(base_addr);
	release_mem_region(res->start, remap_size);
	
	return 0;
}

/* Device Probe function
 * ------------------------------------
 * Get the resource structure from the information in device tree.
 * request the memory region needed for the controller, and map it into
 * kernel virtual memory space. Create character device and register file operations for it.
 */
static int neural_net_probe(struct platform_device *pdev)
{
	int ret = 0;
	
	/* Get & map device registers */
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(&pdev->dev, "No memory resource\n");
		return -ENODEV;
	}
	
	remap_size = res->end - res->start + 1;
	if (!request_mem_region(res->start, remap_size, pdev->name)) {
		dev_err(&pdev->dev, "Cannot request IO\n");
		return -ENXIO;
	}
	
	base_addr = ioremap(res->start, remap_size);
	if (base_addr == NULL) {
		dev_err(&pdev->dev, "Couldn't ioremap memory at 0x%08lx\n", (unsigned long) res->start);
		ret = -ENOMEM;
		goto err_release_region;
	}

	/* IRQ alloc */
	irq_nr = platform_get_irq(pdev, 0);
	if (!irq_nr) {
		dev_err(&pdev->dev, "No IRQ\n");
		ret = -EIO;
		goto err_unmap;
	}
	
	if (devm_request_irq(&pdev->dev, irq_nr, neural_net_inthandler, 0, "neural_net_char", &pdev->dev)) {
		dev_err(&pdev->dev, "Couldn't allocate IRQ\n");
		ret = -EIO;
		goto err_unmap;
	}
	
	char_major = register_chrdev(0, DRIVER_NAME, &neural_net_fops);
	if (char_major < 0) {
		printk(KERN_ALERT "Can't register major number\n");
		goto err_release_irq;
	}
	printk (KERN_INFO "%s: registered character device with major %d\n", DRIVER_NAME, char_major);
	
	neural_net_class = class_create(THIS_MODULE, DRIVER_NAME);
	device_create(neural_net_class, NULL, MKDEV(char_major, 0), NULL, DRIVER_NAME);
	
	neural_net_dev = &pdev->dev;
	buffer_in.pos = 0;
	buffer_in.size = 0;
	buffer_out.pos = 0;
	buffer_out.size = 0;
	
	return 0;
	
err_release_irq:
	devm_free_irq(&pdev->dev, irq_nr, NULL);
err_unmap:
	iounmap(base_addr);
err_release_region:
	release_mem_region(res->start, remap_size);
	
	return ret;
}

/* device match table to match with device node in device tree */
static const struct of_device_id neural_net_of_match[] = {
	{.compatible = "xlnx,neuralnet-1.0"},
	{},
};

MODULE_DEVICE_TABLE(of, neural_net_of_match);

/* platform driver structure for myled driver */
static struct platform_driver neural_net_driver = {
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table = neural_net_of_match},
	.probe = neural_net_probe,
	.remove = neural_net_remove,
};

/* Register myled platform driver */
module_platform_driver(neural_net_driver);

/* Module Infomations */
MODULE_AUTHOR("Petr Elexa");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION(DRIVER_NAME ": Neural network DMA driver");
MODULE_ALIAS(DRIVER_NAME);

