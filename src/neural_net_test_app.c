#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <unistd.h>
#include <stdio.h>

#define DEVICE_NAME "/dev/neuralnet"

/* iotcl requests */
#define NN_RESET			_IO(0xFF, 0)
#define NN_START_COPY		_IO(0xFF, 1)
#define NN_START_TOUPPER	_IO(0xFF, 2)
#define NN_START_NET		_IO(0xFF, 3)
#define NN_READ_IF			_IO(0xFF, 4)
#define NN_ENABLE_IRQ		_IO(0xFF, 5)
#define NN_SET_BURST_R		_IO(0xFF, 6)
#define NN_SET_BURST_W		_IO(0xFF, 7)

#define NN_SET_Y_SIZE 		_IO(0xFF, 8)
#define NN_SET_R_SIZE		_IO(0xFF, 9)
#define NN_SET_L_SIZE		_IO(0xFF, 10)
#define NN_SET_K_HEIGHT		_IO(0xFF, 11)
#define NN_SET_K_WIDTH		_IO(0xFF, 12)
#define NN_SET_NET_MODE		_IO(0xFF, 13)
#define NN_SET_ITERATIONS	_IO(0xFF, 14)
#define NN_R_SIZE			_IO(0xFF, 15)
#define NN_L_SIZE			_IO(0xFF, 16)
#define NN_K_HEIGHT			_IO(0xFF, 17)
#define NN_K_WIDTH			_IO(0xFF, 18)
#define NN_DMA_CLOCKS 		_IO(0xFF, 19)
#define NN_DIAG_ON			_IO(0xFF, 20)
#define NN_DIAG_OFF			_IO(0xFF, 21)

#define BUF_SIZE				1024

int main(int argc, char *argv[])
{
	char buf[BUF_SIZE];
	int i;
	int cnt;
	int fn;
	unsigned int burstszr;
	unsigned int burstszw;
	int fd;
	int ret = 0;
	unsigned int ret_par;
	
	if (argc != 4 
		|| sscanf(argv[1], "%d", &fn) != 1 
		|| sscanf(argv[2], "%u", &burstszr) != 1 
		|| sscanf(argv[3], "%u", &burstszw) != 1)
	{
		printf("Invalid arguments\n");
		return 1;
	}
	
	fd  = open(DEVICE_NAME, O_RDWR);
	if (fd < 0)
	{
		printf("Can't open device\n");
		return 1;
	}
	
	/* Setup */
	ret += ioctl(fd, NN_ENABLE_IRQ, 1);
	ret += ioctl(fd, NN_RESET, BUF_SIZE);
	ret += ioctl(fd, NN_SET_BURST_R, burstszr);
	ret += ioctl(fd, NN_SET_BURST_W, burstszw);
	ret += ioctl(fd, NN_SET_Y_SIZE, 64);
	ret += ioctl(fd, NN_SET_R_SIZE, 4);
	ret += ioctl(fd, NN_SET_NET_MODE, 0x2);
	ret += ioctl(fd, NN_SET_ITERATIONS, 10);

	if (ret < 0)
	{
		fprintf(stderr, "Setup error");
		return 1;
	}

	/* Write buffer to device */
	for (i = 0; i < BUF_SIZE; i++)
		buf[i] = i;

	if (write(fd, buf, BUF_SIZE) < 0)
	{
		printf("Device write error\n");
		return 2;
	}
	
	switch (fn)
	{	
		case 0:
			ret += ioctl(fd, NN_START_COPY);	
			break;
		case 1:
			ret += ioctl(fd, NN_START_TOUPPER);	
			break;
		case 2:
			ret += ioctl(fd, NN_START_NET);
			break;
		case 3:
			ret += ioctl(fd, NN_DIAG_ON);
			
			ret += ioctl(fd, NN_R_SIZE, &ret_par);
			printf("R_SIZE:%hu \n", (unsigned short)ret_par);

			ret += ioctl(fd, NN_L_SIZE, &ret_par);
			printf("L_SIZE:%hu \n", (unsigned short)ret_par);

			ret += ioctl(fd, NN_K_WIDTH, &ret_par);
			printf("K_WIDTH:%hhu \n", (unsigned char)ret_par);

			ret += ioctl(fd, NN_K_HEIGHT, &ret_par);
			printf("K_HEIGHT:%hhu \n", (unsigned char)ret_par);

			ret += ioctl(fd, NN_DMA_CLOCKS, &ret_par);
			printf("DMA_CLOCKS:%u \n", ret_par);
			
			ret += ioctl(fd, NN_DIAG_OFF);
			
			close(fd);
			return -ret;
		default:
			ret += ioctl(fd, NN_START_COPY);	
			break;
	}

	if (ret < 0)
	{
		fprintf(stderr, "Start error");
		return 3;
	}

	/* read result */
	lseek(fd, 0, SEEK_SET);

	i = 0;
	cnt = 0;
	while ((i = read(fd, buf + cnt, BUF_SIZE)) > 0)
	{
		cnt += i;
	}

	if (i < 0)
	{
		printf("Device read error\n");
		return 4;
	}

	/* print result */
	for (i = 0; i < cnt; i++)
	{
		printf("%u ", (unsigned int) buf[i]);
	}
	printf("\n");

	close(fd);

	return 0;
}

