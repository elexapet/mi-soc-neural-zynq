
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use	work.DMA_COMMON.all;

entity DMA_WRITE is
	generic
	(
		DMA_LEN_WIDTH			: integer := 20;
		-- Thread ID Width
		C_M_AXI_ID_WIDTH		: integer := 1;
		-- Width of Address Bus
		C_M_AXI_ADDR_WIDTH		: integer := 32;
		-- Width of Data Bus
		C_M_AXI_DATA_WIDTH		: integer := 32;
		-- Width of User Write Address Bus
		C_M_AXI_AWUSER_WIDTH	: integer := 0;
		-- Width of User Write Data Bus
		C_M_AXI_WUSER_WIDTH		: integer := 0;
		-- Width of User Response Bus
		C_M_AXI_BUSER_WIDTH		: integer := 0
	);
	port
	(
		CLK				: in std_logic;
		RESET			: in std_logic;
		
		-- FIFO signals
		FIFO_DATA		: in std_logic_vector(31 downto 0);
		FIFO_EMPTY		: in std_logic;
		FIFO_NEAR_EMPTY	: in std_logic;
		FIFO_RD			: out std_logic;
		
		-- Core signals
		DMA_INIT		: in std_logic;
		DMA_DONE		: out std_logic;
		DMA_ADDR		: in std_logic_vector(31 downto 0);
		DMA_LEN			: in std_logic_vector(DMA_LEN_WIDTH - 1 downto 0);
		
		MAX_BURSTSZ		: in std_logic_vector(7 downto 0);
		
		-- Debug
		LEDS			: out std_logic_vector(3 downto 0);
		
		-- AXI master write address(AW), write data(W) and write response(B) channels signals
		
		M_AXI_AWID		: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		-- Master Interface Write Address
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		-- Burst length. The burst length gives the exact number of transfers in a burst
		M_AXI_AWLEN		: out std_logic_vector(7 downto 0);
		-- Burst size. This signal indicates the size of each transfer in the burst
		M_AXI_AWSIZE	: out std_logic_vector(2 downto 0);
		-- Burst type. The burst type and the size information, determine how the address for each transfer within the burst is calculated.
		M_AXI_AWBURST	: out std_logic_vector(1 downto 0);
		-- Lock type. Provides additional information about the atomic characteristics of the transfer.
		M_AXI_AWLOCK	: out std_logic;
		-- Memory type. This signal indicates how transactions are required to progress through a system.
		M_AXI_AWCACHE	: out std_logic_vector(3 downto 0);
		-- Protection type. This signal indicates the privilege and security level of the transaction, and whether the transaction is a data access or an instruction access.
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		-- Quality of Service, QoS identifier sent for each write transaction.
		M_AXI_AWQOS		: out std_logic_vector(3 downto 0);
		-- Optional User-defined signal in the write address channel.
		M_AXI_AWUSER	: out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
		-- Write address valid. This signal indicates that the channel is signaling valid write address and control information.
		M_AXI_AWVALID	: out std_logic;
		-- Write address ready. This signal indicates that the slave is ready to accept an address and associated control signals
		M_AXI_AWREADY	: in std_logic;
		-- Master Interface Write Data.
		M_AXI_WDATA		: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte lanes hold valid data. There is one write strobe bit for each eight bits of the write data bus.
		M_AXI_WSTRB		: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		-- Write last. This signal indicates the last transfer in a write burst.
		M_AXI_WLAST		: out std_logic;
		-- Optional User-defined signal in the write data channel.
		M_AXI_WUSER		: out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
		-- Write valid. This signal indicates that valid write data and strobes are available
		M_AXI_WVALID	: out std_logic;
		-- Write ready. This signal indicates that the slave can accept the write data.
		M_AXI_WREADY	: in std_logic;
		-- Master Interface Write Response.
		M_AXI_BID		: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		-- Write response. This signal indicates the status of the write transaction.
		M_AXI_BRESP		: in std_logic_vector(1 downto 0);
		-- Optional User-defined signal in the write response channel
		M_AXI_BUSER		: in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
		-- Write response valid. This signal indicates that the channel is signaling a valid write response.
		M_AXI_BVALID	: in std_logic;
		-- Response ready. This signal indicates that the master can accept a write response.
		M_AXI_BREADY	: out std_logic
	);
end entity;

architecture DMA_WRITE_ARCH of DMA_WRITE is
	
	-- Controller
	-------------
	
	type STATE_T is
	(
		IDLE,
		INIT_DMA,
		INIT_BURST,
		START_BURST,
		TRANSFER,
		WAIT_RESP,
		DONE
	);
	
	signal STATE, NEXT_STATE : STATE_T;
	
	-- Control signals
	signal WRMAXBS		: std_logic;	-- Max burst size reg write enable
	signal WRNAD		: std_logic;
	signal SELNAD		: std_logic;
	signal WRDC 		: std_logic;
	signal SELDC		: std_logic;
	signal WRAD			: std_logic;
	signal WRBS			: std_logic;
	signal WRBC			: std_logic;
	signal SELBC		: std_logic;
	signal BURST_START	: std_logic;
	signal BURST_ACTIVE	: std_logic;
	
	signal FIFO_RD_S	: std_logic;
	
	-- State & data signals
	signal BURST_DONE	: std_logic;

	signal DCZA			: std_logic;
	signal DCZB			: std_logic;
	signal BURSTCNT_1	: std_logic;
	
	signal NADDR		: std_logic_vector(31 downto 0);
	
	signal DMACNT_INPUT	: std_logic_vector(DMA_LEN_WIDTH-1 downto 0);
	signal DMACNT		: std_logic_vector(DMA_LEN_WIDTH-1 downto 0);
	
	signal BURSTSZ		: std_logic_vector(8 downto 0);
	signal AWLEN_INPUT	: std_logic_vector(8 downto 0);
	
	signal BURSTCNT		: std_logic_vector(8 downto 0);
	
	signal AWVALID		: std_logic;
	signal WVALID		: std_logic;
	
	constant MAX_BSZ : std_logic_vector(7 downto 0) := X"80";
	-- signal MAX_BURSTSZ_EXT	: std_logic_vector(8 downto 0);
	signal MAXBS			: std_logic_vector(8 downto 0);

begin

	--============
	-- Controller
	--============
	
	-- State reg
	process (CLK)
	begin
		if rising_edge(CLK) then
			if RESET = '1' then
				STATE <= IDLE;
			else
				STATE <= NEXT_STATE;
			end if;
		end if;
	end process;
	
	-- Transitions
	process (STATE, DMA_INIT, DCZA, DCZB, BURST_DONE, M_AXI_BVALID)
	begin
		case STATE is
			when IDLE =>
				if (DMA_INIT = '1' and DCZA = '0') then
					NEXT_STATE <= INIT_DMA;
				elsif (DMA_INIT = '1' and DCZA = '1') then
					NEXT_STATE <= DONE;
				else
					NEXT_STATE <= IDLE;
				end if;
			when INIT_DMA =>
				NEXT_STATE <= INIT_BURST;
			when INIT_BURST =>
				NEXT_STATE <= START_BURST;
			when START_BURST =>
				NEXT_STATE <= TRANSFER;
			when TRANSFER =>
				if (BURST_DONE = '1') then
					NEXT_STATE <= WAIT_RESP;
				else
					NEXT_STATE <= TRANSFER;
				end if;
			when WAIT_RESP =>
				if (M_AXI_BVALID = '1') then
					if (DCZB = '1') then
						NEXT_STATE <= DONE;
					else
						NEXT_STATE <= INIT_BURST;
					end if;
				else
					NEXT_STATE <= WAIT_RESP;
				end if;
			when DONE =>
				NEXT_STATE <= IDLE;
		end case;
	end process;
	
	-- Outputs
	process (STATE)
	begin
		WRMAXBS			<= '0';
		WRNAD			<= '0';
		SELNAD			<= '0';
		WRDC			<= '0';
		SELDC			<= '0';
		WRAD			<= '0';
		WRBS			<= '0';
		WRBC			<= '0';
		SELBC			<= '0';
		BURST_START		<= '0';
		BURST_ACTIVE	<= '0';
		DMA_DONE		<= '0';
		M_AXI_BREADY	<= '0';
		LEDS(2 downto 0)			<= (others => '0');

		case STATE is
			when IDLE =>
				--null;
				LEDS(2 downto 0)			<= "001";
			when INIT_DMA =>
				WRMAXBS			<= '1';
				WRNAD			<= '1';
				WRDC			<= '1';
				LEDS(2 downto 0)			<= "010";
			when INIT_BURST =>
				WRAD			<= '1';
				WRBS			<= '1';
				SELNAD			<= '1';
				SELDC			<= '1';
				LEDS(2 downto 0)			<= "011";
			when START_BURST =>
				WRNAD			<= '1';
				WRDC			<= '1';
				SELNAD			<= '1';
				SELDC			<= '1';
				WRBC			<= '1';
				BURST_START		<= '1';
				LEDS(2 downto 0)			<= "100";
			when TRANSFER =>
				SELNAD			<= '1';
				SELDC			<= '1';
				SELBC			<= '1';
				BURST_ACTIVE	<= '1';
				LEDS(2 downto 0)			<= "101";
			when WAIT_RESP =>
				SELNAD			<= '1';
				SELDC			<= '1';
				SELBC			<= '1';
				M_AXI_BREADY	<= '1';
				LEDS(2 downto 0)			<= "110";
			when DONE =>
				DMA_DONE		<= '1';
				LEDS(2 downto 0)			<= "111";
		end case;
	end process;
	
	
	-- Protocol logic
	-----------------
	WVALID			<= BURST_ACTIVE and (not FIFO_EMPTY);
	M_AXI_WVALID	<= WVALID;
	M_AXI_WLAST		<= WVALID and BURSTCNT_1;
	M_AXI_WDATA		<= FIFO_DATA;
	
	FIFO_RD			<= FIFO_RD_S;
	FIFO_RD_S		<= WVALID and M_AXI_WREADY;
	BURST_DONE		<= WVALID and M_AXI_WREADY and BURSTCNT_1;
	
	-- Datapath
	-----------
	
	-- AXI address valid (AWVALID)
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				AWVALID <= '0';
			else
				if (AWVALID = '1' and M_AXI_AWREADY = '1') then
					AWVALID <= '0';
				elsif (BURST_START = '1') then
					AWVALID <= '1';
				end if;
			end if;
		end if;
	end process;
	
	M_AXI_AWVALID	<= AWVALID;
	
	-- Next address register with MUX switching between initial DMA address from core module and incremented address
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				NADDR <= (others => '0');
			elsif (WRNAD = '1') then
				if (SELNAD = '0') then
					NADDR <= DMA_ADDR;
				else
					NADDR <= std_logic_vector(unsigned(NADDR) + unsigned(BURSTSZ & "00"));
					
					-- !!! pri pouziti SHIFT_LEFT dojde k preteceni 9-bitoveho signalu, je-li hodnota >= 128
					-- NADDR <= std_logic_vector(unsigned(NADDR) + SHIFT_LEFT(unsigned(BURSTSZ), 2));
				end if;
			end if;
		end if;
	end process;
	
	-- AXI Address register
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				M_AXI_AWADDR <= (others => '0');
			elsif (WRAD = '1') then
				M_AXI_AWADDR <= NADDR;
			end if;
		end if;
	end process;
	
	-- DMA counter
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				DMACNT <= (others => '0');
			elsif (WRDC = '1') then
				DMACNT <= DMACNT_INPUT;
			end if;
		end if;
	end process;
	
	DMACNT_INPUT	<= DMA_LEN when SELDC = '0' else std_logic_vector(unsigned(DMACNT) - unsigned(BURSTSZ));
	DCZA			<= '1' when unsigned(DMACNT_INPUT) = 0 else '0';
	DCZB			<= '1' when unsigned(DMACNT) = 0 else '0';
	
	-- Max burst size
	
	-- MAX_BURSTSZ_EXT(8) <= '0';
	-- MAX_BURSTSZ_EXT(7 downto 0) <= MAX_BURSTSZ;
	
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				MAXBS <= (others => '0');
			elsif (WRMAXBS = '1') then
				-- BURSTSZ <= std_logic_vector(to_unsigned(get_burst_size(NADDR, DMACNT), 9));
				-- MAXBS <= std_logic_vector(unsigned(MAX_BURSTSZ_EXT) + 1);
				MAXBS <= std_logic_vector(unsigned('0' & MAX_BURSTSZ) + 1);
			end if;
		end if;
	end process;
	
	-- Burst size
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				BURSTSZ <= (others => '0');
			elsif (WRBS = '1') then
				-- BURSTSZ <= std_logic_vector(to_unsigned(get_burst_size(NADDR, DMACNT), 9));
				BURSTSZ <= GET_BURST_SIZE(NADDR, DMACNT, MAXBS);
				-- BURSTSZ <= GET_BURST_SIZE(NADDR, DMACNT, MAX_BSZ);
			end if;
		end if;
	end process;
	
	-- Write burst length (AWLEN)
	AWLEN_INPUT	<= std_logic_vector(unsigned(BURSTSZ) - 1);
	M_AXI_AWLEN	<= AWLEN_INPUT(7 downto 0);
	
	-- Burst counter
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				BURSTCNT <= (others => '0');
			elsif (WRBC = '1' or FIFO_RD_S = '1') then
				if (SELBC = '1') then
					BURSTCNT <= std_logic_vector(unsigned(BURSTCNT) - 1);
				else
					BURSTCNT <= BURSTSZ;
				end if;
			end if;
		end if;
	end process;
	
	BURSTCNT_1	<= '1' when unsigned(BURSTCNT) = 1 else '0';
	
	
	-- Write response error detection
	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				LEDS(3) <= '0';
			elsif (M_AXI_BRESP(1) = '1') then
				LEDS(3) <= '1';
			end if;
		end if;
	end process;
	
	-- 2^SIZE = 4 B in each transfer
	M_AXI_AWSIZE	<= "010";
	-- Burst type = INCR
	M_AXI_AWBURST	<= "01";
	--Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache. 
	M_AXI_AWCACHE	<= "0010";
	--All bursts are complete and aligned
	M_AXI_WSTRB		<= (others => '1');
	
	-- Unused/default AXI signals
	M_AXI_AWID		<= (others => '0');
	M_AXI_AWLOCK	<= '0';
	M_AXI_AWPROT	<= "000";
	M_AXI_AWQOS		<= x"0";
	M_AXI_AWUSER	<= (others => '1');
	M_AXI_WUSER		<= (others => '0');

end architecture;

-- vim: set nocindent autoindent:

