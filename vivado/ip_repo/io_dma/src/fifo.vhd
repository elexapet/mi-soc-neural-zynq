
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FIFO is
	generic
	(
		SIZE_BITS	: integer := 4;
		DATA_WIDTH	: integer := 32
	);
	port
	(
		CLK			: in std_logic;
		RESET		: in std_logic;
		DATA_IN		: in std_logic_vector(DATA_WIDTH - 1 downto 0);
		DATA_OUT	: out std_logic_vector(DATA_WIDTH - 1 downto 0);
		-- control signals
		WR			: in std_logic;
		RD			: in std_logic;
		-- status signals
		EMPTY		: out std_logic;
		NEAR_EMPTY	: out std_logic;
		FULL		: out std_logic;
		NEAR_FULL	: out std_logic;
		LEDS		: out std_logic_vector(3 downto 0)
	);
end entity;

architecture FIFO_ARCH of FIFO is

	type DATA_T is array(0 to 2 ** SIZE_BITS - 1) of std_logic_vector(DATA_WIDTH - 1 downto 0);

	signal DATA			: DATA_T;
	signal HEAD, TAIL	: std_logic_vector(SIZE_BITS - 1 downto 0);
	-- signal HEAD, TAIL	: natural range 0 to 2 ** SIZE_BITS - 1; -- wrap doesn't work
	signal EMPTY_S, NEAR_EMPTY_S, FULL_S, NEAR_FULL_S	: std_logic;

begin
	
	LEDS(0)		<= EMPTY_S;
	LEDS(1)		<= NEAR_EMPTY_S;
	LEDS(2)		<= FULL_S;
	LEDS(3)		<= NEAR_FULL_S;
	
	EMPTY		<= EMPTY_S;
	NEAR_EMPTY	<= NEAR_EMPTY_S;
	FULL		<= FULL_S;
	NEAR_FULL	<= NEAR_FULL_S;
	
	EMPTY_S			<= '1' when (HEAD = TAIL) else '0';
	NEAR_EMPTY_S	<= '1' when (HEAD = std_logic_vector(unsigned(TAIL) - 1)) else '0';
	FULL_S			<= '1' when (HEAD = std_logic_vector(unsigned(TAIL) + 1)) else '0';
	NEAR_FULL_S		<= '1' when (HEAD = std_logic_vector(unsigned(TAIL) + 2)) else '0';
	
	DATA_OUT	<= DATA(to_integer(unsigned(HEAD)));

	process (CLK)
	begin
		if rising_edge(CLK) then
			if (RESET = '1') then
				HEAD <= (others => '0');
				TAIL <= (others => '0');
			else
				if (RD = '1' and EMPTY_S = '0') then
					HEAD <= std_logic_vector(unsigned(HEAD) + 1);
				end if;

				if (WR = '1' and FULL_S = '0') then
					DATA(to_integer(unsigned(TAIL))) <= DATA_IN;
					TAIL <= std_logic_vector(unsigned(TAIL) + 1);
				end if;
			end if;
		end if;
	end process;

end architecture;

-- vim: set nocindent autoindent:

