
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package DMA_COMMON is
	function GET_BURST_SIZE(ADDRESS, DMA_LEN, MAX_BURST_SIZE: std_logic_vector) return std_logic_vector;
end DMA_COMMON;

package body DMA_COMMON is

	function GET_BURST_SIZE(ADDRESS, DMA_LEN, MAX_BURST_SIZE: std_logic_vector) return std_logic_vector is
		constant addr			: natural range 0 to 4095 := to_integer(unsigned(ADDRESS(11 downto 0)));
		constant len			: natural := to_integer(unsigned(DMA_LEN));
		constant max_bsz		: natural range 1 to 256 := to_integer(unsigned(MAX_BURST_SIZE));
		variable x				: natural range 0 to 1024;
		variable ret			: natural range 0 to 256;
	begin
		x := (4096 - addr) / 4;
		if (len > max_bsz) then
			ret := max_bsz;
			
			if (x < max_bsz) then
				ret := x;
			else
				ret := max_bsz;
			end if;
		else
			ret := len;
			if (x < len) then
				ret := x;
			else
				ret := len;
			end if;
		end if;
		return std_logic_vector(to_unsigned(ret, 9));
	end function;

end DMA_COMMON;
