
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity TOP is
	generic
	(
		-- Users to add parameters here
		DMA_LEN_WIDTH		: integer := 20;
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Master Bus Interface M00_AXI
		--C_M00_AXI_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
		--C_M00_AXI_BURST_LEN	: integer	:= 16;
		C_M00_AXI_ID_WIDTH	: integer	:= 1;
		C_M00_AXI_ADDR_WIDTH	: integer	:= 32;
		C_M00_AXI_DATA_WIDTH	: integer	:= 32;
		C_M00_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_M00_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_M00_AXI_WUSER_WIDTH	: integer	:= 0;
		C_M00_AXI_RUSER_WIDTH	: integer	:= 0;
		C_M00_AXI_BUSER_WIDTH	: integer	:= 0;

		-- Parameters of Axi Slave Bus Interface S01_AXI
		C_S01_AXI_DATA_WIDTH	: integer	:= 32;
		C_S01_AXI_ADDR_WIDTH	: integer	:= 5
	);
	port (
		-- Users to add ports here
		IRQ					: out std_logic;
		LEDS				: out std_logic_vector(3 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Master Bus Interface M00_AXI
		m00_axi_aclk	: in std_logic;
		m00_axi_aresetn	: in std_logic;
		m00_axi_awid	: out std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
		m00_axi_awaddr	: out std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
		m00_axi_awlen	: out std_logic_vector(7 downto 0);
		m00_axi_awsize	: out std_logic_vector(2 downto 0);
		m00_axi_awburst	: out std_logic_vector(1 downto 0);
		m00_axi_awlock	: out std_logic;
		m00_axi_awcache	: out std_logic_vector(3 downto 0);
		m00_axi_awprot	: out std_logic_vector(2 downto 0);
		m00_axi_awqos	: out std_logic_vector(3 downto 0);
		m00_axi_awuser	: out std_logic_vector(C_M00_AXI_AWUSER_WIDTH-1 downto 0);
		m00_axi_awvalid	: out std_logic;
		m00_axi_awready	: in std_logic;
		m00_axi_wdata	: out std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
		m00_axi_wstrb	: out std_logic_vector(C_M00_AXI_DATA_WIDTH/8-1 downto 0);
		m00_axi_wlast	: out std_logic;
		m00_axi_wuser	: out std_logic_vector(C_M00_AXI_WUSER_WIDTH-1 downto 0);
		m00_axi_wvalid	: out std_logic;
		m00_axi_wready	: in std_logic;
		m00_axi_bid	: in std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
		m00_axi_bresp	: in std_logic_vector(1 downto 0);
		m00_axi_buser	: in std_logic_vector(C_M00_AXI_BUSER_WIDTH-1 downto 0);
		m00_axi_bvalid	: in std_logic;
		m00_axi_bready	: out std_logic;
		m00_axi_arid	: out std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
		m00_axi_araddr	: out std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
		m00_axi_arlen	: out std_logic_vector(7 downto 0);
		m00_axi_arsize	: out std_logic_vector(2 downto 0);
		m00_axi_arburst	: out std_logic_vector(1 downto 0);
		m00_axi_arlock	: out std_logic;
		m00_axi_arcache	: out std_logic_vector(3 downto 0);
		m00_axi_arprot	: out std_logic_vector(2 downto 0);
		m00_axi_arqos	: out std_logic_vector(3 downto 0);
		m00_axi_aruser	: out std_logic_vector(C_M00_AXI_ARUSER_WIDTH-1 downto 0);
		m00_axi_arvalid	: out std_logic;
		m00_axi_arready	: in std_logic;
		m00_axi_rid	: in std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
		m00_axi_rdata	: in std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
		m00_axi_rresp	: in std_logic_vector(1 downto 0);
		m00_axi_rlast	: in std_logic;
		m00_axi_ruser	: in std_logic_vector(C_M00_AXI_RUSER_WIDTH-1 downto 0);
		m00_axi_rvalid	: in std_logic;
		m00_axi_rready	: out std_logic;

		-- Ports of Axi Slave Bus Interface S01_AXI
		s01_axi_aclk	: in std_logic;
		s01_axi_aresetn	: in std_logic;
		s01_axi_awaddr	: in std_logic_vector(C_S01_AXI_ADDR_WIDTH-1 downto 0);
		s01_axi_awprot	: in std_logic_vector(2 downto 0);
		s01_axi_awvalid	: in std_logic;
		s01_axi_awready	: out std_logic;
		s01_axi_wdata	: in std_logic_vector(C_S01_AXI_DATA_WIDTH-1 downto 0);
		s01_axi_wstrb	: in std_logic_vector((C_S01_AXI_DATA_WIDTH/8)-1 downto 0);
		s01_axi_wvalid	: in std_logic;
		s01_axi_wready	: out std_logic;
		s01_axi_bresp	: out std_logic_vector(1 downto 0);
		s01_axi_bvalid	: out std_logic;
		s01_axi_bready	: in std_logic;
		s01_axi_araddr	: in std_logic_vector(C_S01_AXI_ADDR_WIDTH-1 downto 0);
		s01_axi_arprot	: in std_logic_vector(2 downto 0);
		s01_axi_arvalid	: in std_logic;
		s01_axi_arready	: out std_logic;
		s01_axi_rdata	: out std_logic_vector(C_S01_AXI_DATA_WIDTH-1 downto 0);
		s01_axi_rresp	: out std_logic_vector(1 downto 0);
		s01_axi_rvalid	: out std_logic;
		s01_axi_rready	: in std_logic
	);
end TOP;

architecture arch_imp of TOP is

	component DMA_WRITE is
	generic
	(
		DMA_LEN_WIDTH			: integer := 20;
		C_M_AXI_ID_WIDTH		: integer := 1;
		C_M_AXI_ADDR_WIDTH		: integer := 32;
		C_M_AXI_DATA_WIDTH		: integer := 32;
		C_M_AXI_AWUSER_WIDTH	: integer := 0;
		C_M_AXI_WUSER_WIDTH		: integer := 0;
		C_M_AXI_BUSER_WIDTH		: integer := 0
	);
	port
	(
		CLK				: in std_logic;
		RESET			: in std_logic;
		
		FIFO_DATA		: in std_logic_vector(31 downto 0);
		FIFO_EMPTY		: in std_logic;
		FIFO_NEAR_EMPTY	: in std_logic;
		FIFO_RD			: out std_logic;
		
		DMA_INIT		: in std_logic;
		DMA_DONE		: out std_logic;
		DMA_ADDR		: in std_logic_vector(31 downto 0);
		DMA_LEN			: in std_logic_vector(DMA_LEN_WIDTH - 1 downto 0);
		
		MAX_BURSTSZ		: in std_logic_vector(7 downto 0);
		
		LEDS			: out std_logic_vector(3 downto 0);
		
		M_AXI_AWID		: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_AWLEN		: out std_logic_vector(7 downto 0);
		M_AXI_AWSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_AWBURST	: out std_logic_vector(1 downto 0);
		M_AXI_AWLOCK	: out std_logic;
		M_AXI_AWCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		M_AXI_AWQOS		: out std_logic_vector(3 downto 0);
		M_AXI_AWUSER	: out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
		M_AXI_AWVALID	: out std_logic;
		M_AXI_AWREADY	: in std_logic;
		M_AXI_WDATA		: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_WSTRB		: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		M_AXI_WLAST		: out std_logic;
		M_AXI_WUSER		: out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
		M_AXI_WVALID	: out std_logic;
		M_AXI_WREADY	: in std_logic;
		M_AXI_BID		: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_BRESP		: in std_logic_vector(1 downto 0);
		M_AXI_BUSER		: in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
		M_AXI_BVALID	: in std_logic;
		M_AXI_BREADY	: out std_logic
	);
	end component DMA_WRITE;
	
	component DMA_READ is
	generic
	(
		DMA_LEN_WIDTH			: integer := 20;
		C_M_AXI_ID_WIDTH		: integer := 1;
		C_M_AXI_ADDR_WIDTH		: integer := 32;
		C_M_AXI_DATA_WIDTH		: integer := 32;
		C_M_AXI_ARUSER_WIDTH	: integer := 0;
		C_M_AXI_RUSER_WIDTH		: integer := 0
	);
	port
	(
		CLK				: in std_logic;
		RESET			: in std_logic;
		
		FIFO_DATA		: out std_logic_vector(31 downto 0);
		FIFO_FULL		: in std_logic;
		FIFO_NEAR_FULL	: in std_logic;
		FIFO_WR			: out std_logic;
		
		DMA_INIT		: in std_logic;
		DMA_DONE		: out std_logic;
		DMA_ADDR		: in std_logic_vector(31 downto 0);
		DMA_LEN			: in std_logic_vector(DMA_LEN_WIDTH - 1 downto 0);
		
		MAX_BURSTSZ		: in std_logic_vector(7 downto 0);
		
		LEDS			: out std_logic_vector(3 downto 0);
		
		M_AXI_ARID		: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_ARADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_ARLEN		: out std_logic_vector(7 downto 0);
		M_AXI_ARSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_ARBURST	: out std_logic_vector(1 downto 0);
		M_AXI_ARLOCK	: out std_logic;
		M_AXI_ARCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_ARPROT	: out std_logic_vector(2 downto 0);
		M_AXI_ARQOS		: out std_logic_vector(3 downto 0);
		M_AXI_ARUSER	: out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
		M_AXI_ARVALID	: out std_logic;
		M_AXI_ARREADY	: in std_logic;
		M_AXI_RID		: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_RDATA		: in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_RRESP		: in std_logic_vector(1 downto 0);
		M_AXI_RLAST		: in std_logic;
		M_AXI_RUSER		: in std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
		M_AXI_RVALID	: in std_logic;
		M_AXI_RREADY	: out std_logic
	);
	end component DMA_READ;
	
	component FIFO is
	generic
	(
		SIZE_BITS	: integer := 4;
		DATA_WIDTH	: integer := 32
	);
	port
	(
		CLK			: in std_logic;
		RESET		: in std_logic;
		DATA_IN		: in std_logic_vector(DATA_WIDTH - 1 downto 0);
		DATA_OUT	: out std_logic_vector(DATA_WIDTH - 1 downto 0);
		
		WR			: in std_logic;
		RD			: in std_logic;
		
		EMPTY		: out std_logic;
		NEAR_EMPTY	: out std_logic;
		FULL		: out std_logic;
		NEAR_FULL	: out std_logic;
		
		LEDS		: out std_logic_vector(3 downto 0)
	);
	end component FIFO;
	
	component CORE is
	generic
	(
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 5;
		DMA_LEN_WIDTH	: integer := 20
	);
	port
	(
		-- AXI Slave interface
		S_AXI_ACLK		: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA		: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB		: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP		: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA		: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP		: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic;

		-- Core ports
		CLK					: in std_logic;
		RESET				: in std_logic;

		IRQ					: out std_logic;
		LEDS				: out std_logic_vector(3 downto 0);

		-- Interface to DMA Write
		W_DMA_INIT			: out std_logic;
		W_DMA_DONE			: in std_logic;
		W_DMA_ADDR			: out std_logic_vector(31 downto 0);
		W_DMA_LEN			: out std_logic_vector(DMA_LEN_WIDTH - 1 downto 0);
		MAXBS_W				: out std_logic_vector(7 downto 0);

		-- Interface to DMA Read
		R_DMA_INIT			: out std_logic;
		R_DMA_DONE			: in std_logic;
		R_DMA_ADDR			: out std_logic_vector(31 downto 0);
		R_DMA_LEN			: out std_logic_vector(DMA_LEN_WIDTH - 1 downto 0);
		MAXBS_R				: out std_logic_vector(7 downto 0);

		-- Interface to Write FIFO
		W_FIFO_DIN			: out std_logic_vector(31 downto 0);
		W_FIFO_WR			: out std_logic;
		W_FIFO_FULL			: in std_logic;
		W_FIFO_NEAR_FULL	: in std_logic;

		-- Interface to Read FIFO
		R_FIFO_DOUT			: in std_logic_vector(31 downto 0);
		R_FIFO_RD			: out std_logic;
		R_FIFO_EMPTY		: in std_logic;
		R_FIFO_NEAR_EMPTY	: in std_logic	
	);
	end component CORE;

	-- Active-high reset
	signal POS_RESET_M			: std_logic;
	signal POS_RESET_S			: std_logic;

	-- Core <--> Write FIFO	
	signal W_FIFO_DIN			: std_logic_vector(31 downto 0);	-- Core to Write FIFO
	signal W_FIFO_WR			: std_logic;						-- Core -> W_FIFO
	signal W_FIFO_FULL			: std_logic;						-- W_FIFO -> Core
	signal W_FIFO_NEAR_FULL		: std_logic;						-- W_FIFO -> Core

	-- Write FIFO <--> DMA_WRITE
	signal W_FIFO_DOUT			: std_logic_vector(31 downto 0);	-- Write FIFO to DMA_WRITE
	signal W_FIFO_RD			: std_logic;						-- DMA_WRITE -> W_FIFO
	signal W_FIFO_EMPTY			: std_logic;						-- W_FIFO -> DMA_WRITE
	signal W_FIFO_NEAR_EMPTY	: std_logic;						-- W_FIFO -> DMA_WRITE

	-- Core <--> Read FIFO
	signal R_FIFO_DOUT			: std_logic_vector(31 downto 0);	-- Read FIFO to Core
	signal R_FIFO_RD			: std_logic;						-- Core -> R_FIFO
	signal R_FIFO_EMPTY			: std_logic;						-- R_FIFO -> Core
	signal R_FIFO_NEAR_EMPTY	: std_logic;						-- R_FIFO -> Core

	-- Read FIFO <--> DMA_READ	
	signal R_FIFO_DIN			: std_logic_vector(31 downto 0);	-- DMA_READ to Read FIFO
	signal R_FIFO_WR			: std_logic;						-- DMA_READ -> R_FIFO
	signal R_FIFO_FULL			: std_logic;						-- R_FIFO -> DMA_READ
	signal R_FIFO_NEAR_FULL		: std_logic;						-- R_FIFO -> DMA_READ
	
	-- Core <--> DMA_WRITE
	signal W_DMA_INIT			: std_logic;
	signal W_DMA_DONE			: std_logic;
	signal W_DMA_ADDR			: std_logic_vector(31 downto 0);
	signal W_DMA_LEN			: std_logic_vector(DMA_LEN_WIDTH - 1 downto 0);
	signal MAXBS_W				: std_logic_vector(7 downto 0);

	-- Core <--> DMA_READ
	signal R_DMA_INIT			: std_logic;
	signal R_DMA_DONE			: std_logic;
	signal R_DMA_ADDR			: std_logic_vector(31 downto 0);
	signal R_DMA_LEN			: std_logic_vector(DMA_LEN_WIDTH - 1 downto 0);
	signal MAXBS_R				: std_logic_vector(7 downto 0);

begin
	
	W_DMA : DMA_WRITE
	generic map
	(
		DMA_LEN_WIDTH			=> DMA_LEN_WIDTH,
		C_M_AXI_ID_WIDTH		=> C_M00_AXI_ID_WIDTH,
		C_M_AXI_ADDR_WIDTH		=> C_M00_AXI_ADDR_WIDTH,
		C_M_AXI_DATA_WIDTH		=> C_M00_AXI_DATA_WIDTH,
		C_M_AXI_AWUSER_WIDTH	=> C_M00_AXI_AWUSER_WIDTH,
		C_M_AXI_WUSER_WIDTH		=> C_M00_AXI_WUSER_WIDTH,
		C_M_AXI_BUSER_WIDTH		=> C_M00_AXI_BUSER_WIDTH
	)
	port map
	(
		CLK				=> m00_axi_aclk,
		RESET			=> POS_RESET_M,
		
		FIFO_DATA		=> W_FIFO_DOUT,
		FIFO_EMPTY		=> W_FIFO_EMPTY,
		FIFO_NEAR_EMPTY	=> W_FIFO_NEAR_EMPTY,
		FIFO_RD			=> W_FIFO_RD,
		
		DMA_INIT		=> W_DMA_INIT,
		DMA_DONE		=> W_DMA_DONE,
		DMA_ADDR		=> W_DMA_ADDR,
		DMA_LEN			=> W_DMA_LEN,
		
		MAX_BURSTSZ		=> MAXBS_W,
		
		LEDS			=> LEDS,
		
		M_AXI_AWID		=> m00_axi_awid,
		M_AXI_AWADDR	=> m00_axi_awaddr,
		M_AXI_AWLEN		=> m00_axi_awlen,
		M_AXI_AWSIZE	=> m00_axi_awsize,
		M_AXI_AWBURST	=> m00_axi_awburst,
		M_AXI_AWLOCK	=> m00_axi_awlock,
		M_AXI_AWCACHE	=> m00_axi_awcache,
		M_AXI_AWPROT	=> m00_axi_awprot,
		M_AXI_AWQOS		=> m00_axi_awqos,
		M_AXI_AWUSER	=> m00_axi_awuser,
		M_AXI_AWVALID	=> m00_axi_awvalid,
		M_AXI_AWREADY	=> m00_axi_awready,
		M_AXI_WDATA		=> m00_axi_wdata,
		M_AXI_WSTRB		=> m00_axi_wstrb,
		M_AXI_WLAST		=> m00_axi_wlast,
		M_AXI_WUSER		=> m00_axi_wuser,
		M_AXI_WVALID	=> m00_axi_wvalid,
		M_AXI_WREADY	=> m00_axi_wready,
		M_AXI_BID		=> m00_axi_bid,
		M_AXI_BRESP		=> m00_axi_bresp,
		M_AXI_BUSER		=> m00_axi_buser,
		M_AXI_BVALID	=> m00_axi_bvalid,
		M_AXI_BREADY	=> m00_axi_bready
	);

	W_FIFO : FIFO
	generic map
	(
		SIZE_BITS	=> 5,
		DATA_WIDTH	=> 32
	)
	port map
	(
		CLK			=> m00_axi_aclk,
		RESET		=> POS_RESET_M,

		DATA_IN		=> W_FIFO_DIN,
		DATA_OUT	=> W_FIFO_DOUT,
		
		WR			=> W_FIFO_WR,
		RD			=> W_FIFO_RD,
		
		EMPTY		=> W_FIFO_EMPTY,
		NEAR_EMPTY	=> W_FIFO_NEAR_EMPTY,
		FULL		=> W_FIFO_FULL,
		NEAR_FULL	=> W_FIFO_NEAR_FULL
		
		--LEDS		=> LEDS
	);

	R_DMA : DMA_READ
	generic map
	(
		DMA_LEN_WIDTH			=> DMA_LEN_WIDTH,
		C_M_AXI_ID_WIDTH		=> C_M00_AXI_ID_WIDTH,
		C_M_AXI_ADDR_WIDTH		=> C_M00_AXI_ADDR_WIDTH,
		C_M_AXI_DATA_WIDTH		=> C_M00_AXI_DATA_WIDTH,
		C_M_AXI_ARUSER_WIDTH	=> C_M00_AXI_ARUSER_WIDTH,
		C_M_AXI_RUSER_WIDTH		=> C_M00_AXI_RUSER_WIDTH
	)
	port map
	(
		CLK				=> m00_axi_aclk,
		RESET			=> POS_RESET_M,
		
		FIFO_DATA		=> R_FIFO_DIN,
		FIFO_FULL		=> R_FIFO_FULL,
		FIFO_NEAR_FULL	=> R_FIFO_NEAR_FULL,
		FIFO_WR			=> R_FIFO_WR,
		
		DMA_INIT		=> R_DMA_INIT,
		DMA_DONE		=> R_DMA_DONE,
		DMA_ADDR		=> R_DMA_ADDR,
		DMA_LEN			=> R_DMA_LEN,
		
		MAX_BURSTSZ		=> MAXBS_R,
		
		--LEDS			=> LEDS,
		
		M_AXI_ARID		=> m00_axi_arid,
		M_AXI_ARADDR	=> m00_axi_araddr,
		M_AXI_ARLEN		=> m00_axi_arlen,
		M_AXI_ARSIZE	=> m00_axi_arsize,
		M_AXI_ARBURST	=> m00_axi_arburst,
		M_AXI_ARLOCK	=> m00_axi_arlock,
		M_AXI_ARCACHE	=> m00_axi_arcache,
		M_AXI_ARPROT	=> m00_axi_arprot,
		M_AXI_ARQOS		=> m00_axi_arqos,
		M_AXI_ARUSER	=> m00_axi_aruser,
		M_AXI_ARVALID	=> m00_axi_arvalid,
		M_AXI_ARREADY	=> m00_axi_arready,
		M_AXI_RID		=> m00_axi_rid,
		M_AXI_RDATA		=> m00_axi_rdata,
		M_AXI_RRESP		=> m00_axi_rresp,
		M_AXI_RLAST		=> m00_axi_rlast,
		M_AXI_RUSER		=> m00_axi_ruser,
		M_AXI_RVALID	=> m00_axi_rvalid,
		M_AXI_RREADY	=> m00_axi_rready
	);

	R_FIFO : FIFO
	generic map
	(
		SIZE_BITS	=> 5,
		DATA_WIDTH	=> 32
	)
	port map
	(
		CLK			=> m00_axi_aclk,
		RESET		=> POS_RESET_M,

		DATA_IN		=> R_FIFO_DIN,
		DATA_OUT	=> R_FIFO_DOUT,
		
		WR			=> R_FIFO_WR,
		RD			=> R_FIFO_RD,
		
		EMPTY		=> R_FIFO_EMPTY,
		NEAR_EMPTY	=> R_FIFO_NEAR_EMPTY,
		FULL		=> R_FIFO_FULL,
		NEAR_FULL	=> R_FIFO_NEAR_FULL
	);

	CORE_INST : CORE
	generic map
	(
		C_S_AXI_DATA_WIDTH	=> C_S01_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S01_AXI_ADDR_WIDTH,
		DMA_LEN_WIDTH		=> DMA_LEN_WIDTH
	)
	port map
	(
		-- map to S01 axi
		S_AXI_ACLK			=> s01_axi_aclk,
		S_AXI_ARESETN		=> s01_axi_aresetn,
		S_AXI_AWADDR		=> s01_axi_awaddr,
		S_AXI_AWPROT		=> s01_axi_awprot,
		S_AXI_AWVALID		=> s01_axi_awvalid,
		S_AXI_AWREADY		=> s01_axi_awready,
		S_AXI_WDATA			=> s01_axi_wdata,
		S_AXI_WSTRB			=> s01_axi_wstrb,
		S_AXI_WVALID		=> s01_axi_wvalid,
		S_AXI_WREADY		=> s01_axi_wready,
		S_AXI_BRESP			=> s01_axi_bresp,
		S_AXI_BVALID		=> s01_axi_bvalid,
		S_AXI_BREADY		=> s01_axi_bready,
		S_AXI_ARADDR		=> s01_axi_araddr,
		S_AXI_ARPROT		=> s01_axi_arprot,
		S_AXI_ARVALID		=> s01_axi_arvalid,
		S_AXI_ARREADY		=> s01_axi_arready,
		S_AXI_RDATA			=> s01_axi_rdata,
		S_AXI_RRESP			=> s01_axi_rresp,
		S_AXI_RVALID		=> s01_axi_rvalid,
		S_AXI_RREADY		=> s01_axi_rready,

		CLK					=> m00_axi_aclk,
		RESET				=> POS_RESET_M,

		IRQ					=> IRQ,
		--LEDS				=> LEDS,

		W_DMA_INIT			=> W_DMA_INIT,
		W_DMA_DONE			=> W_DMA_DONE,
		W_DMA_ADDR			=> W_DMA_ADDR,
		W_DMA_LEN			=> W_DMA_LEN,

		R_DMA_INIT			=> R_DMA_INIT,
		R_DMA_DONE			=> R_DMA_DONE,
		R_DMA_ADDR			=> R_DMA_ADDR,
		R_DMA_LEN			=> R_DMA_LEN,

		W_FIFO_DIN			=> W_FIFO_DIN,
		W_FIFO_WR			=> W_FIFO_WR,
		W_FIFO_FULL			=> W_FIFO_FULL,
		W_FIFO_NEAR_FULL	=> W_FIFO_NEAR_FULL,

		R_FIFO_DOUT			=> R_FIFO_DOUT,
		R_FIFO_RD			=> R_FIFO_RD,
		R_FIFO_EMPTY		=> R_FIFO_EMPTY,
		R_FIFO_NEAR_EMPTY	=> R_FIFO_NEAR_EMPTY
	);
	
	POS_RESET_M	<= (not m00_axi_aresetn);
	POS_RESET_S	<= (not s01_axi_aresetn);

end arch_imp;

-- vim: set nocindent autoindent:
