## Project goal

Simple recurrent neural network implementation in FPGA with interface to ARM processor on Zynq SoC.

## Project purpose

Demo application for Zybo board that will use neural network in hardware to recognize hand-written numerals from camera images.

## Specification

See attached specification file or presentation

## What is what

The "src" folder contains linux driver and example use of the driver.

The "vivado" is Vivado project in VHDL containing system for the Zynq. The neural network with AXI interface, DMA controllers and FIFOs packaged as IP core that is used in the system.

## Team members

Petr Elexa (elexapet@fit.cvut.cz) 

David Jagoš (jagosdav@fit.cvut.cz)

Carlos Escuín Blasco (escuicar@fit.cvut.cz)